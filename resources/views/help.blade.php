<!DOCTYPE html>
<html>
    <head>
        
        <title>WebTracker V2 :: ContentMedia/CrawlerIndex</title>
        
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/app.css" />
        
    </head>
    <body id="mainPage" class="page">
        
        <div class="container">
            <div class="content">
                
                <figure class="userForm">
                    
                    <header id="pageHeader" class="header">
                        <a href="/" title="WebTracker - Home" class="appBrand">
                            <h1 class="appName">
                                WebTracker v2
                            </h1>
                        </a>
                        <strong class="appSlogan">What does the Cloud know about you...?</strong>
                    </header>
                    
                    <section id="widgetArea" class="widget interact">
                        <section class="mainContent">
                            <h2>WebTracker Manual</h2>
                            <hr />
                            <p>A short summary about all options will be shown here, soon...</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <a href="/" title="Back">
                                <p>[Go back to main page]</p>
                            </a>
                        </section>
                    </section>
                    
                    <footer id="pageFooter" class="footer">
                        <section class="normalContent">
                            <p>(c)2016 Created by Arjuna Noorsanto :: LOOPSHAPE Media Development</p>
                        </section>
                    </footer>
                    
                </figure>
                
            </div>
        </div>
        
        <script src="./../webtracker/node_modules/requirejs/require.js" data-main="./../webtracker/resources/assets/js/main"></script>
               
    </body>
</html>