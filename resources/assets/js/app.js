/*property
actId, baseUrl, buffer, command, config, crdate, date, getDate,
getFullYear, getHours, getMinutes, getMonth, getSeconds, getTime, hits, id,
jquery, log, modded, name, paths, play, pointer, query, rank, request,
requirejs, response, result, saved, shim, stack, status, typed
*/
//BOF
'use strict';
console.log('WebTracker V2\nCreated by Arjuna Noorsanto\nLOOPSHAPE :: Media Development');
var WebTracker = function(WebTracker) {
    /*
    * WEBTRACKER APP
    * Javascript coded by: Arjuna Noorsanto
    * eMail: awebgo.net@gmail.com
    */
    // REQUIREJS
    require.config({
        baseUrl : '/webtracker/resources/assets/js',
        paths : {
            'requirejs' : './../../../bower_components/requirejs/require',
            'jquery' : './../../../bower_components/jquery2/jquery.min',
            'jquery-migrate' : './../../../bower_components/jquery2/jquery-migrate.min'
        },
        shim : {}
    });
    // GLOBAL JAVASCRIPT
    var _this = this,
        App = require(['jquery', 'jquery-migrate'], function($) {
        // PROGRESS START
        var context = {
            // LOCALIZATION
            vector : {
                'id' : {},
                'pointer' : {},
                'actId' : {},
                'name' : {},
                // FACTORS
                'rank' : {},
                'hits' : {},
                'crdate' : {},
                'modded' : {},
                'date' : {},
                // CONTEXT
                'stack' : {},
                'buffer' : {},
                'query' : {},
                'command' : {},
                'play' : {},
                // RESPONSE
                'request' : {},
                'response' : {},
                'recorded' : {},
                'result' : {},
                'status' : {},
                'typed' : {},
                'saved' : {}
            }
        },
            tagArray = {
            vector : {
                'local' : {},
                'remote' : {},
                'pos' : {},
                'name' : {
                    regEx : {
                        'inject' : {},
                        'element' : {},
                        'html' : {}
                    }
                }
            }
        };
        // BASE =>  Math.PI || 16 || 10 || 8 || 2 || 1 || 1/2 || 1/3725 || 0 ||
        // limes || alpha || beta || gamma
        var sng = sng ? 1 ? 0 : 1 : 0,
            _PI = 2 * Math.PI;
        var alpha = alpha ? alpha : sng,
            beta = sng ? 1 : 0,
            gamma = Math.cos(alpha),
            delta = beta * alpha ? Math.sin(alpha) === 0 : alpha,
            omega = delta ? alpha : -alpha,
            zeta = Math.atan(beta * sng) * alpha;
        var _n = _n * sng,
            _alpha = (Math.sin(_PI * sng) * 360 + 1) / _n,
            Base = {
            'set' : alpha && gamma,
            'def' : beta,
            'get' : beta && gamma,
            'tri' : 2 * ((_PI + alpha) * _get),
            'sin' : Math.sin(alpha),
            'cos' : Math.cos(alpha),
            'bin' : _n * _n * _get,
            'oct' : function(sng, bin, oct) {
                return function(oct) {
                    oct = oct !== 3 ? sng + bin + oct : 1;
                    return this;
                };
            }
        };
        // SETUP ENVIRONMENT
        var ready = true;
        _alpha = _alpha === alpha ? alpha : _alpha;
        var _beta = _beta === beta ? beta : _beta;
        gamma = beta !== 0;
        var drop = drop ? flow = ' * ' : flow = ' : ',
            flow = drop === '' ? flow : flow = ' ! ',
        // COMMUNICATION
            req = {},
            res = {},
        // MEMORIES
            rep = {},
            rem = {},
        // PLAY
            fix = {},
            mem = {},
        // ACTIVITY
            opcode = {
            'commands' : {
                'set' : /^set/,
                'get' : /^get/,
                'def' : /^def/,
                'load' : /^load/,
                'save' : /^save/
            }
        };
        // REFRESH TIME
        Base, opcode, work, rep, _defO, direction, _one, regExCode, regExStd, regExSpc, regExChO, statusfix_source, statusfix_target, statusSet, hexVar, regReq, regRes, regCb, debug, repeater, dataBase, table, name, fixture, memory, remark, trackerData, temper, hashtag, tagcmd, tagbrk, tagcnt, tagsts, taggrp, tagget, tagdiz, statusData, $userBuffer, setParameters, pusher, userField, typus, getQuery, serverResponse,
        _this = null;
        // ACTIVITY MEMORY RELATION
        var response = cb;
        var _setI = !response;
        var _getI = response;
        var _set = _setI ? res !== req : req !== stack;
        var _setO = parseInt(1 * _set);
        var _defI = _setO || _getO;
        var _getO = parseInt(truth * _set);
        var _defO = _getO === _setO ? 1 : undefined;
        var _get = true;
        var que = 1;
        _setI = true;
        _get = _get ? _set : _setI;
        var truth = null === _defI ? _setI : _getI === response;
        var direction = _get || _set;
        delta = sng === 0 ? Math.cos(alpha) : Math.sin(alpha);
        var bin = sng === 1 ? 1 : 0;
        omega = sng && bin;
        var oct = sng * bin * oct && sng * bin * context.vector.length > 0,
            dual = sng === 1 && bin === 1 && oct === (sng || bin || oct),
            base = sng && bin && dual === true;
        zeta = sng * bin * oct * base;
        var vec = zeta ? _get : vec || vec !== 0 ? vec = _get : vec = _set;
        fix = sng === base || dual || oct || bin || sng || sng === 0 || _PI;
        var _zero = fix ? fix : alpha,
            _one = _zero !== true ? beta : gamma,
        // LOCATION MEMORY RELATION
            movex = movex + parseInt(1 * _set),
            movey = movey + parseInt(1 * _get),
        // MOVEMENT
            regExCode = /(set|get|def|clr|upd|reset|load|save)?/,
            regExStd = /^(.+)?$/,
            regExSpc = /^\ $/,
            regExChO = /^.$/,
        // BACKEND VARS
            id = 1,
        // SETUP
            actId = 1,
            pointer = 1,
            director = 1,
            source = {},
        // REQUEST
            target = {},
            statusfix_source =
            source,
            statusfix_target = target ? true : null,
            presskey = false,
        // RESPONSE
            cb = cb ? cb : true,
            DB = {},
        // CALLBACK
            $preZero = '@000000',
            vector =
            vec,
            power = 0,
        // BASE
            exec = false,
            enter = false,
            clear = false,
            date = '00/00/0000',
        // RESULT
            time = '00:00:00' + _set * (' - ' + date),
            timestamp = '0000000000000',
            crdate =
            timestamp,
            modded =
            crdate,
            clocktimer = {},
            freeze = false,
            gotorun = false,
            counter = 0,
        // THE LOOK INTO THE PRESENT
            setTime = function() {
            date = new Date();
            timestamp = date.getTime();
            time = '' + (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()) + ' - ' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '/' + (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '/' + date.getFullYear();
            return time;
        },
        // ACTING TYPE :: NOMEN
            typed = {
            0 : 'Data',
            1 : 'Object',
            2 : 'Group',
            3 : 'Instance',
            4 : 'Motivation',
            5 : 'Communication',
            6 : 'Knowledge',
            7 : 'Gameplay',
            8 : 'Awareness',
            9 : 'Constitution',
            10 : 'League',
            11 : 'Special',
            12 : 'Awesome',
            13 : 'Perfect',
            14 : 'Universal',
            15 : 'Absolute'
        },
            typefix = 0,
        // OBJECT :: VERB
            verb = {
            '&' : 'is calling',
            '=' : 'does behave like',
            '*' : 'matched with',
            '<' : 'defines the',
            '>' : 'describes the',
            '@' : 'is related to',
            '#' : 'was tagged by',
            '+' : 'has been involved with',
            '^' : 'comes with the',
            's' : 'is stacked by',
            'b' : 'is buffered for',
            'c' : 'was executed `cause of',
            'q' : 'was parsed by',
            '!' : 'is like',
            '?' : 'has conflicts with',
            ':' : 'and is related to',
            '/' : 'wasn`t found without'
        },
            verbId = 'S',
        // SUBJECT :: FEATURE
            object = {
            '&' : 'Name',
            '=' : 'of',
            '*' : 'Information',
            '<' : 'Definition',
            '>' : 'Description',
            '@' : 'Relation',
            '#' : 'Hashtag',
            '+' : 'Object',
            '^' : 'Vector',
            's' : 'Stack',
            'b' : 'Buffer',
            'c' : 'Command',
            'q' : 'Query',
            '!' : 'Status',
            '?' : 'Event',
            ':' : 'Date',
            '/' : 'Time'
        },
            objId = '&',
        // CLASS OF DEFINITION
            definer = {
            true : 'right',
            false : 'wrong',
            null : 'empty',
            undefined : 'didn`t match',
            1 : 'best relation',
            2 : 'good behavior',
            3 : 'doesn`t know',
            4 : 'fixed',
            5 : 'cleared',
            6 : 'deleted',
            7 : 'kicked',
            8 : 'banned',
            9 : 'missed',
            10 : 'resetted'
        },
            feature = {
            0 : 'Object Item',
            1 : 'Object Name',
            2 : 'Object Category',
            3 : 'Object Group',
            4 : 'Item Section',
            5 : 'Item Family',
            6 : 'Family Package',
            7 : 'Package Brand',
            8 : 'Brand Company',
            9 : 'Company World',
            10 : 'World Universe',
            11 : 'Universe Galaxy',
            12 : 'Galaxy Cosmos'
        },
            featfix = 0,
            firstRun = true,
            status = {
            0 : 'OFF',
            1 : 'OK',
            2 : 'ON',
            3 : 'SET',
            4 : 'GET',
            5 : 'CNT',
            6 : 'BRK',
            7 : 'REP',
            8 : 'PRG',
            9 : 'STS',
            10 : 'ERR',
            11 : 'NOP',
            12 : 'GO',
            13 : 'EMP',
            14 : 'BIT',
            15 : 'CMD',
            16 : 'RST',
            17 : 'CLR',
            18 : 'DEL',
            19 : 'MVD',
            20 : 'FIX'
        },
            statusfix = 1,
            statusSet = false,
            hexVar = {
            0 : '0',
            1 : '1',
            2 : '2',
            3 : '3',
            4 : '4',
            5 : '5',
            6 : '6',
            7 : '7',
            8 : '8',
            9 : '9',
            a : 'a',
            b : 'b',
            c : 'c',
            d : 'd',
            e : 'e',
            f : 'f'
        },
            appStr = '',
            defStr = '',
            preStr = '',
            regReq = {},
            regRes = {},
            regEx = {},
            regCb = {},
            scSet = 0,
            content = {
            vector : {
                'id' : {},
                'vector' : {},
                'actId' : {},
                'name' : {},
                // FACTORS
                'rank' : {},
                'hits' : {},
                'crdate' : {},
                'modded' : {},
                'date' : {},
                // CONTEXT
                'stack' : {},
                'buffer' : {},
                'query' : {},
                'command' : {},
                'play' : {},
                // RESPONSE
                'request' : {},
                'response' : {},
                'result' : {},
                'status' : {},
                'typed' : {},
                'saved' : {},
                // LOGIC MAINFRAME :: START
                'warp' : {
                    0 : null,
                    1 : true,
                    3 : false,
                    4 : undefined,
                    5 : 0,
                    6 : 1,
                    7 : 2,
                    8 : 3,
                    9 : 4,
                    10 : object[objId],
                    11 : verb[verbId],
                    12 : feature[featfix],
                    13 : typed[typefix],
                    14 : status[statusfix],
                    15 : req.req !== res.res,
                    true : true,
                    false : false,
                    null : null,
                    undefined : undefined
                },
                tagArray : {
                    ti : {
                        'local' : feature[featfix],
                        'remote' : status[statusfix],
                        'internet' : object[objId],
                        'script' : {
                            '<' : '<div class="layoutBox">',
                            '>' : '</div>',
                            '{' : '<script rel="javascript">',
                            '}' : '</script>'
                        }
                    }
                }
            }
        },
        // GETTING THE GLOBAL-INDEX
            play = play ? statusfix = 1 : statusfix = 11,
            ground = status[play] || 0,
            debug = ground !== 0 ? ground : status[ground],
            request = cb ? request : debug,
            repeater = false,
            _html = false,
            _css = false,
            _js = false;
        response = response ? cb === true : cb === false;
        var dataset = {};
        var request = request ? req : '',
            result = result ? res : req,
        // THE LOOK INTO THE PAST
            dataBase =
            content,
            table = target || null,
            savemode = false,
            name = '',
            stack = {},
            buffer = {},
            command = {},
            fixture = {},
            memory = {},
            remark = {},
            recognized = false,
            trackerData = {},
            query = object.q,
            enter = !enter ? true : false,
            truth =
            undefined,
            exec = cb ? true : false,
            dataset = {
            'cb' : cb,
            'req' : !cb ? null : req,
            'res' : !cb ? null : res
        };
        var temper = false,
            complete = complete ? complete : false,
            rank = rank ? rank + 1 * _getO : rank || 1,
            hits = hits ? hits + 1 * _setO : hits || 1,
            timestamp = context.vector.name,
            crdate = context.vector.crdate,
            modded = context.vector.modded;
        var saved = context.vector.saved,
            hid = hid ? hid + 1 : hid,
            hashcloud = hashcloud + '#TagGroup',
            hashstack = hashstack + '#TagCount',
            hashtag = '#TagName',
            hashindex = hashindex + 1,
            tagcmd = 3,
        // LEGEND
            tagbrk = 2,
        // LIFETIME
            tagcnt = 40,
        // MIDLIFE
            tagsts = 12,
        // THE YEAR
            taggrp = 4.275,
        // THE MONTH
            tagset = 7,
        // THE DAYS
            tagdef = 24,
        // THE HOURS
            tagget = 60,
        // THE MINUTES
            tagdiz = 60,
        // THE SECONDS
            tagc = tagc ? tagc : 0,
        // THE NOW
            dateset = false,
            urlset = false,
        // STRATEGIC VARS
            statusData = {
            0 : '',
            1 : 'Showing the Data-Table ... ',
            2 : 'Opening given time ... ',
            3 : 'Opening URL location ... ',
            4 : 'Executing the Query !!! ',
            5 : 'Variable setup successful ...'
        },
        // MARKETING VARS
            stringData = {
            0 : 'What does the Cloud know about you...?',
            1 : 'Type `help` for the manual!',
            2 : 'Application coded by: Arjuna Noorsanto',
            3 : 'Contact eMail: awebgo.net@gmail.com',
            4 : 'Github Repository: https://github.com/Loopshape/webtracker.git',
            5 : '(c)2016 by LOOPSHAPE::Media Development'
        },
        // MAINFRAME QUERY VARS
            singleSpc = true,
            singleRet = false,
            singleTab = false,
            singleDel = false,
        // FRONTEND VARS :: DATA CONCLUSION
            $userBuffer = {},
            $userField = $('input#mainQuery'),
            $table = $('.infoTable'),
            $appSlogan = $('.appSlogan'),
            $box = $('.queryBox'),
        // PAGE ELEMENTS :: OBJECT DEFINITIONS
            $elemId = $table.find('.id span'),
            $elemPointer = $table.find('.pointer span'),
            $elemActId = $table.find('.actId span'),
            $elemName = $table.find('.name span'),
            $elemRank = $table.find('.rank span'),
            $elemTyped = $table.find('.typed span'),
            $elemStatus = $table.find('.status span'),
            $elemHits = $table.find('.hits span'),
            $elemCrdate = $table.find('.crdate span'),
            $elemModded = $table.find('.modded span'),
            $elemDate = $table.find('.date span'),
            $elemStack = $table.find('.stack span'),
            $elemBuffer = $table.find('.buffer span'),
            $elemQuery = $table.find('.query span'),
            $elemCommand = $table.find('.command span'),
            $elemPlay = $table.find('.play span'),
            $elemRequest = $table.find('.request span'),
            $elemResponse = $table.find('.response span'),
            $elemResult = $table.find('.result span'),
            $elemSaved = $table.find('.saved span'),
            tp = context.vector + actId * (statusfix > 0 && statusfix < context.length),
            hi = ci || tp || statusfix,
            ci = hi || tp || statusfix,
            ti = ci === hi ? statusfix === 1 : recognized = cb,
            warp = status[typefix],
        // THE LOOK INTO THE FUTURE
            controllerIndex = function(pointer, cb, que) {
            if (!cb || !context.vector)
                return normal();
            context.vector.crdate = crdate = setTime();
            context.vector.saved = saved = context.vector.length !== 0;
            id = actId = context.vector = id + pointer;
            return normal();
        },
        // DATA CONCLUSION
            setContext = function(req, cb, res, rem, fix, mem, sts, pointer) {
            /*
            if (!cb || !context.vector)
            return normal();
            */
            // START ARCHIVEMENT :: GLOBAL STAGE
            context.vector = actId;
            clocktimer = {},
            freeze = false,
            id = context.vector;
            actId = id + pointer,
            dual = counter / 2 === parseInt(counter / 2) ? 1 : 0,
            clear = counter === 0 ? true : false,
            power = id === actId ? counter : id + pointer,
            gotorun = !freeze,
            presskey = '', // PREDICTED DATABASE
            $preZero =
            actId,
            $preZero = actId < 10 ? '0' + actId : $preZero,
            $preZero = actId < 100 ? '00' + actId : $preZero,
            $preZero = actId < 1000 ? '000' + actId : $preZero,
            $preZero = actId < 10000 ? '0000' + actId : $preZero,
            $preZero = actId < 100000 ? '00000' + actId : $preZero,
            $preZero = actId < 1000000 ? '00000' + actId : $preZero,
            director =
            pointer,
            _get = !_set ? context.vector.response : target.actId,
            _set = _set ? _get : false,
            _setI = parseInt(_setI) !== _setI,
            _setO = _set * _getO,
            _get = !_set * _getI,
            _getI = res * _setI,
            _getO = req * _defO;
            var _def = req + 1 * ( _setO = _set === _get),
                _defO = req * _def,
                _defI = res * _set;
            _set = _defI !== false && _defI !== 0 ? id + pointer : id - pointer;
            if (_set)
                _get = true;
            context.vector.response = _set;
            context = {
                vector : {
                    'id' : actId,
                    'object' : object[objId],
                    'vector' : pointer,
                    'actId' : $preZero,
                    'name' : _get ? typed[typefix] : '@' + $preZero,
                    // FACTORS
                    'rank' : rank,
                    'hits' : hits,
                    'crdate' : crdate,
                    'modded' : modded ? setTime() : modded,
                    'date' : setTime(),
                    // CONTEXT
                    'stack' : stack,
                    'buffer' : stack !== '' && buffer,
                    'query' : query,
                    'command' : command,
                    'play' : play,
                    // RESPONSE
                    'request' : request || res,
                    'response' : response === res + '' ? '' : {},
                    'result' : response / 2 === parseInt(response / 2) ? true : false,
                    'dual' : response / 2 === parseInt(response / 2) ? true : false,
                    'status' : response ? true : false,
                    'typed' : typed[typefix],
                    'saved' : saved,
                    'warp' : {
                        'even' : true,
                        'odd' : false
                    }
                }
            };
            // WATCHER :: STOP
            if (context.vector !== id) {
                source = context;
                vector = id + pointer;
                target = source;
            }
            controllerIndex(pointer, cb, que);
            DB = context || target || source;
            result = DB;
            return normal();
        },
        // TRACKER::START :: LOCAL STAGE
            start = function(req, cb, res) {
            counter = 0;
            setInterval(function(time, freeze) {
                time = freeze ? setTime() : time;
                $appSlogan.html(stringData[Math.random() * stringData.length + 1]);
                counter++;
            }, 1000);
            Parse(counter);
            return normal();
        },
        // USER INTERACTION
            Parse = function(counter) {
            Meta(counter);
            return normal();
        },
        // CREATE THE OBJECT-MODEL :: REMOTE STAGE
            setParameters = function(req, cb, res, rem, fix, mem, sts, pointer, que) {
            actId = id + pointer === context.vector ? id + pointer : actId;
            if (context.vector.length !== _set || actId === undefined) {
                que = _get;
            }
            if (!que)
                return normal();
            // GET DEFAULT PARAMETERS
            actId = que ? pointer : actId;
            setContext(req, cb, res, rem, fix, mem, sts, pointer);
            source = result;
            id = context.vector;
            pointer = context.vector;
            actId = pointer + id;
            context.vector.pointer = parseInt(pointer);
            // DEFINE RESULT FOR COMMAND
            context.vector = target.actId;
            setContext(req, cb, res, rem, fix, mem, sts, pointer);
            target = result;
            // WRITE PARAMETERS INTO DATABase
            result = target;
            cb = !cb ? true : false;
            return normal();
        };
        var sts = status[statusfix],
        // SUBSEQUENCES
            showTable = function(_css) {
            if (_css === undefined) {
                $table.css('display', ($table.prop('display') === 'none' ? true : false) ? 'inline-block' : 'none');
            } else {
                $table.css('display', ( _css ? true : false) ? 'inline-block' : 'none');
            }
            res = $table.prop('display');
            return normal();
        },
            renderTable = function(_set) {
            result = setContext(req, cb, res, rem, fix, mem, sts, pointer);
            if (_set && result) {
                context = result;
                if ($table.length && context.vector) {
                    $elemId.html(context.vector);
                    $elemPointer.html(context.vector.pointer);
                    $elemActId.html(context.vector.actId);
                    $elemName.html(context.vector.name);
                    $elemRank.html(context.vector.rank);
                    $elemTyped.html(context.vector.typed);
                    $elemStatus.html(context.vector.status);
                    $elemHits.html(context.vector.hits);
                    $elemCrdate.html(context.vector.crdate);
                    $elemModded.html(context.vector.modded);
                    $elemDate.html(context.vector.date);
                    $elemStack.html(context.vector.stack);
                    $elemBuffer.html(context.vector.buffer);
                    $elemQuery.html(context.vector.query);
                    $elemCommand.html(context.vector.command);
                    $elemPlay.html(context.vector.play);
                    $elemRequest.html(context.vector.request);
                    $elemResponse.html(context.vector.response);
                    $elemResult.html(context.vector.result);
                    $elemStatus.html(context.vector.status);
                    $elemSaved.html(context.vector.saved);
                }
                showTable(complete);
            }
            return normal();
        },
        // RETURN TO NORM
            normal = function() {
            return {
                'req' : req,
                'cb' : cb,
                'res' : !cb ? req : res,
                'rem' : rem,
                'fix' : fix,
                'mem' : mem,
                'sts' : sts
            };
        },
        // COMMAND PARSE :: PROCEDURE :: START
            comm = function(req) {
            req = setter(req).req;
            cb = setter(cb).cb;
            res = setter(res).res;
            rem = setter(rem).rem;
            fix = setter(fix).fix;
            mem = setter(mem).mem;
            sts = setter(sts).sts;
            if (cb)
                cb = getStatus(req).cb;
            return normal();
        },
        // EXECUTE PARSED COMMAND
            work = function(req) {
            req = getter(req).req;
            cb = getter(cb).cb;
            res = getter(res).res;
            rem = getter(rem).rem;
            fix = getter(fix).fix;
            mem = getter(mem).mem;
            sts = getter(sts).sts;
            if (cb)
                cb = getStatus(res).cb;
            return normal();
        },
            pusher = function(req) {
            cb = cb ? cb : false;
            req = cb ? req : cb ? req : sts;
            res = cb ? res : req;
            context.vector = id;
            target.actId = id + pointer;
            return normal();
        },
            adder = function(req, cb, res) {
            if (cb) {
                cb = $userField.prop('value', (req + res)) === true;
            }
            return normal();
        },
            checker = function(req) {
            if (context === undefined) {
                cb = false;
            } else {
                cb = actId > 0 && actId < context.length ? true : false;
                res = cb ? req : cb;
                rem = res ? context.vector : {};
                fix = rem === res;
                if (cb && fix) {
                    mem = rem;
                }
            }
            return normal();
        },
            getter = function(req) {
            if (context === undefined) {
                return normal();
            }
            if (cb)
                req = context.vector.request;
            if (cb && req)
                res = context.vector.response;
            if (cb && req && res)
                rem = context.vector.remark;
            if (cb && req && res && rem)
                fix = context.vector.fixture;
            if (cb && req && res && rem && fix)
                mem = context.vector.memory;
            if (cb && req && res && rem && fix && mem)
                sts = cb && req && res && rem && fix && mem;
            if (!sts)
                cb = false;
            return normal();
        },
            setter = function(req) {
            if (context === undefined) {
                return normal();
            }
            if (cb)
                context.vector.request = req;
            if (cb && req)
                context.vector.response = res;
            if (cb && req && res)
                context.vector.remark = rem;
            if (cb && req && res && rem)
                context.vector.fixture = fix;
            if (cb && req && res && rem && fix)
                context.vector.memory = mem;
            if (cb && req && res && rem && fix && mem)
                sts = cb && req && res && rem && fix && mem;
            if (!sts)
                cb = false;
            return normal();
        },
        // MODIFY DATA-STATEMENTS
            userField = function(req) {
            cb = getStatus(pointer).cb;
            res = cb ? req : '';
            if (cb && res !== '')
                $userField.prop('value', res);
            return res;
        },
            typus = function(req) {
            res = 10 - 6 * cb;
            return res;
        },
        // SERVER RESPONSE
            getQuery = function(req) {
            req = setContext(req, cb, res, rem, fix, mem, sts, pointer).res;
            res = status[10 - 9 * context.vector.response];
            return res;
        },
            getStatus = function(req) {
            if (cb) {
                res = 10 - 9 * cb;
                $userField.prop('placeholder', status[ req ? req : res]).prop('value', '');
            }
            return normal();
        },
            serverResponse = function(req) {
            actId = context.vector;
            id = context.vector;
            cb = setContext().cb;
            fix = !cb ? cb : fix;
            if (fix && cb) {
                fix = fix ? res : req;
                rem = fix ? req : res;
                mem = fix ? res : res;
                sts = rem !== mem;
            }
            return normal();
        },
        // ************************************************************
        // QUERY ENVIRONMENT VARS
            Meta = function(counter) {
            typefix = 0;
            // START OF INPUT-QUERY
            clear = true;
            singleSpc = clear;
            singleRet = !singleSpc;
            singleDel = !singleSpc;
            drop = true;
            flow = drop ? flow : '';
            stack = $userField.prop('value', flow);
            // WRITE CONTEXT-STATUS
            var define = false;
            exec = false;
            flow = _set ? ' | ' : ' * ';
            statusfix = 13 - 12 * response;
            $userField.on('focus', function(e) {
                e.preventDefault();
                // ************************************************************
                // GET THE KEYBOARD-ENTRIES
                stack = '';
                $userField.prop('value', stack);
                return false;
            }).on('input', function(e) {
                // PREVENT SPECIAL KEYS
                e.preventDefault();
                // LOWLEVEL PARSER STATUS-MESSAGE
                $userField.on('keydown', function(e) {
                    var key = e.keyCode || e.charCode;
                    // KEY::SPACEBAR
                    if (key == 32) {
                        e.preventDefault();
                        if (!singleSpc)
                            singleSpc = true;
                        singleDel = true;
                        singleTab = false;
                        singleRet = false;
                        adder(stack, cb, (stack + flow));
                    }
                    // KEY::RETURN
                    if (key == 13) {
                        e.preventDefault();
                        clear = true;
                        singleSpc = false;
                        singleDel = false;
                        singleTab = false;
                        singleRet = true;
                        // CHECK REQUEST
                        exec = userField('');
                        if (exec) {
                            actId = id;
                            stack = $userField.prop('value');
                            buffer = stack;
                            enter = true;
                        }
                    }
                    // KEY::TABULATOR
                    if (key == 9 || key == 11) {
                        e.preventDefault();
                        clear = true;
                        id = actId = pointer = 1;
                        statusfix = 1;
                        singleSpc = false;
                        singleDel = false;
                        singleRet = false;
                        singleTab = complete = true;
                        renderTable(complete);
                        typefix = 0;
                        featfix = 0;
                        getStatus(status[statusfix]);
                        statusfix = 1;
                        exec = userField(status[statusfix] + ' > ');
                    }
                    // KEY::BACKSPACE
                    if (key == 8) {
                        if (!singleDel) {
                            e.preventDefault();
                            clear = true;
                            exec = false;
                            userField('');
                        }
                        singleSpc = false;
                        singleTab = false;
                        singleRet = false;
                        complete = false;
                        renderTable(complete);
                        typefix = 0;
                        getStatus(buffer);
                    }
                });
                // ************************************************************
                if (stack !== undefined) {
                    stack = '' + toString(setContext(req, cb, res, rem, fix, mem, sts, pointer).req);
                    // QUERY-EXPRESSION HANDLER
                    if (stack.match(/^\+$/)) {
                        dataset = true;
                        singleDel = false;
                        userField('[' + pointer + '] ' + (!define ? '>' : '<') + ' ');
                    }
                    if (stack.match(/^\[(\d)\]\ \>\ ?$/)) {
                        dataset = true;
                        complete = true;
                        singleDel = false;
                        userField('[' + pointer + '] ' + (!define ? '>' : '<') + ' ');
                    }
                    // OPEN DATETIME
                    if (stack.match(/^\*$/)) {
                        dateset = true;
                        userField('* [ ' + setTime() + ' ] = ');
                    }
                    // OPEN URL
                    if (stack.match(/^\/\/$/) && !urlset) {
                        urlset = true;
                        userField('http://');
                    }
                    // GET HASHTAG
                    if (stack.match(/^\#$/) && !tagset) {
                        tagset = tagdef = true;
                        userField('#');
                    }
                    // SAVE HASHTAG
                    if (stack.match(/^\#(.+)?\ $/) && tagset && tagdef) {
                        typefix = 3;
                        regEx = stack.match(/^\#(.+)\ $/)[1] || '';
                        if (regEx !== '') {
                            tagArray = {
                                vector : {
                                    'local' : feature[featfix],
                                    'remote' : status[statusfix],
                                    'pos' : tp,
                                    'name' : {
                                        regEx : {
                                            'inject' : '<span class="tag ' + regEx + '">' + regEx + '</span>',
                                            'element' : object[objId],
                                            'html' : {
                                                '<' : '<div class="layoutBox">',
                                                '>' : '</div>',
                                                '(' : '<script rel="javascript">',
                                                ')' : '</script>',
                                                '{' : '<style rel="stylesheet">',
                                                '}' : '</style>'
                                            }
                                        }
                                    }
                                }
                            };

                            tp++;
                            getStatus('> Hashtag saved');
                            regEx = '';
                            userField('');
                        }
                        ti++;
                        tagdef = false;
                    }
                    // WRITE HASHTAGS
                    if (tp !== 0) {
                        setContext(req, cb, res, rem, fix, mem, sts, pointer);
                        context = result;
                    }
                    for ( tagc = 0; tagc < tagArray.length; tagc++) {
                        $box.find('p.match').html(tagArray[tagc].html);
                    }
                    // GET SHORTCUT
                    if (stack.match(/^.$/)) {
                        scSet = 1;
                        regEx = stack.match(/^(.+)$/)[scSet || 0];
                        if (regEx !== '' && object[regEx] !== undefined) {
                            preStr = regEx === '#' ? '#' : '';
                            defStr = feature[featfix] + ' ' + verb['>'] + ' ' + object[regEx] + ' ' + verb['='] + ' ';
                            appStr = '';
                        }
                        warp = getStatus(preStr + defStr + appStr).res;
                    }
                    // ************************************************************
                    // QUERY RESPONSE STATUS
                    if (define && !vector && !singleRet) {
                        vector = true;
                        singleDel = true;
                        userField('[' + pointer + '] = ');
                    }
                    if (define && vector && singleRet) {
                        singleDel = false;
                        getStatus('[' + pointer + '] \xA6 ' + warp);
                        userField('');
                    }
                    if (define && !savemode) {
                        savemode = true;
                        define == false;
                        query = checker(request).request;
                        objId = verbId;
                        warp = ' ' + verbId + ' ' + definer[typefix] + ' ' + typed[typefix] + ' ' + verb[verbId] + ' ' + (statusfix !== 1 ? 'no' : '') + ' ' + definer[response] + ' ' + object[objId] + ' > ' + status[statusfix] + ' ';
                        console.log('> ' + warp);
                        // TRIGGER DATATABLE
                        renderTable(complete);
                        userField(warp);
                    }
                    // ************************************************************
                    // THE QUERY REQUEST
                    if (enter && cb) {
                        enter = false;
                        // COMMAND PARSER
                        exec = false;
                        // COMMAND INTERPRETER
                        command = savemode ? buffer !== '' ? buffer : '' : command;
                        // REFRESH QUERY WITH RESULT RELATION
                        request = command || '';
                        query = request || '';
                        switch (query) {
                        case 'go':
                            verbId = '@';
                            break;
                        case 'get':
                            verbId = '!';
                            break;
                        case 'set':
                            verbId = '*';
                            break;
                        case 'def':
                            verbId = '/';
                            break;
                        case 'load':
                            verbId = '>';
                            break;
                        case 'save':
                            verbId = '<';
                            break;
                        case 'tst':
                            verbId = '^';
                            break;
                        default:
                            verbId = '?';
                            break;
                        }
                    }
                    // ************************************************************
                    // RESET QUERY ENVIRONMENT
                    define = dataset = tagset = tagdef = false;
                    // SET READY STATUS FOR NEXT STATEMENT
                    ready = !ready || actId !== pointer ? ready = true : ready = false;
                    // GOODBYE
                    if (ready)
                        firstRun = false;
                    // SAVE STACK TO BUFFER
                    buffer = stack;
                    context.vector = actId;
                    context.vector.name = typed[typefix];
                    context.vector.stack = request;
                    context.vector.buffer = result;
                    // CLEAR QUERY-ARRAYS
                    if (complete && singleRet) {
                        drop = complete = singleRet = singleTab = define = urlset = dateset = dataset = clear = vector = firstRun = false;
                    }
                    // RESET QUERY STATUSVARS
                    preStr = defStr = appStr = '';
                    // END QUERY PARSE PROCEDURE :: STOP
                }
            }).on('blur', function(e) {
                // SET IDLE STATUS
                e.preventDefault();
                drop = true;
                flow = stack;
                stack = userField('');
                // WRITE CONTEXT-STATUS
                statusfix = 0;
                getStatus(status[statusfix] + ' < Interpreter offline !');
            });
            $(window).on('focus', function(e) {
                // SET FOCUSED STATUS
                e.preventDefault();
                // SET READY STATUS FOR NEXT STATEMENT
                ready = recognized ? true : false;
                // WRITE CONTEXT-STATUS
                statusfix = 5;
                getStatus(status[statusfix] + ' < Welcome back ...');
                $userField.focus();
            });
            $(document).ready(function(ev) {
                // SET READY STATUS
                statusfix = 2;
                getStatus(status[statusfix] + ' < Interpreter online !');
                // WRITE CONTEXT-STATUS
                $userField.focus();
            });
        };
        start(req, cb, res);
    });
    window.WebTracker = App;
    window.WebTracker();
};
// APP CALL
WebTracker();
//EOF