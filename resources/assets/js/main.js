require.config({
    baseUrl : '/webtracker/resources/assets/js',
    paths : {
        'requirejs' : './../../../bower_components/requirejs/require',
        'jquery' : './../../../bower_components/jquery2/jquery.min'
    },
    shim : {}
});
var $;
var Main = require(['jquery'], function($) {
    require(['config', 'app']);
});
Main(); 