/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.copy('resources/assets/js/**/*.js', 'public/js');
    mix.copy(
        [
            'resources/assets/img/**/*.jpg',
            'resources/assets/img/**/*.png',
            'resources/assets/img/**/*.gif',
            'resources/assets/img/**/*.ico'
        ], 'public/img');
    mix.less('app.less', 'public/css', { paths: 'resources/assets/less/**/*.less' });
});

